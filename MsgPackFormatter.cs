﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using MsgPack;
using MsgPack.Serialization;

namespace WebAPI.Code {

    public class MsgPackFormatter : BufferedMediaTypeFormatter {

        private readonly ConcurrentDictionary<Type, IMessagePackSerializer> _serializersCacheDictionary =
            new ConcurrentDictionary<Type, IMessagePackSerializer>();

        public MsgPackFormatter() {
            SupportedMediaTypes.Add( new MediaTypeHeaderValue( "application/msgpack" ) );
        }

        public override bool CanReadType( Type type ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }

            if ( type.Name == typeof( IEnumerable<> ).Name ) {
                type = typeof( object[] );
            }

            return _serializersCacheDictionary.GetOrAdd( type, t => MessagePackSerializer.Create( type ) ) != null;
        }

        public override bool CanWriteType( Type type ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }

            if ( type.Name == typeof( IEnumerable<> ).Name ) {
                type = typeof( object[] );
            }

            return _serializersCacheDictionary.GetOrAdd( type, t => MessagePackSerializer.Create( type ) ) != null;
        }

        public override object ReadFromStream( Type type, Stream readStream, HttpContent content,
            IFormatterLogger formatterLogger ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }
            if ( readStream == null ) {
                throw new ArgumentNullException( "readStream" );
            }

            var unpacker = Unpacker.Create( readStream );
            var packingObject = unpacker.Unpack<object>();

            return packingObject;
        }

        public override void WriteToStream( Type type, object value, Stream writeStream, HttpContent content ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }
            if ( value == null ) {
                throw new ArgumentNullException( "value" );
            }
            if ( writeStream == null ) {
                throw new ArgumentNullException( "writeStream" );
            }

            var packer = Packer.Create( writeStream );

            packer.PackObject( value );

            writeStream.Flush();

        }
    }

}