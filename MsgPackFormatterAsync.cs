﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using MsgPack;
using MsgPack.Serialization;

namespace WebAPI.Code {

    public class MsgPackFormatterAsync : MediaTypeFormatter {

        private readonly ConcurrentDictionary<Type, IMessagePackSerializer> _serializersCacheDictionary =
            new ConcurrentDictionary<Type, IMessagePackSerializer>();

        public MsgPackFormatterAsync() {
            SupportedMediaTypes.Add( new MediaTypeHeaderValue( "application/msgpack" ) );
        }

        public override bool CanReadType( Type type ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }

            if ( type.Name == typeof( IEnumerable<> ).Name ) {
                type = typeof( object[] );
            }

            return _serializersCacheDictionary.GetOrAdd( type, t => MessagePackSerializer.Create( type ) ) != null;
        }

        public override bool CanWriteType( Type type ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }

            if ( type.Name == typeof ( IEnumerable<> ).Name ) {
                type = typeof ( object[] );
            }

            return _serializersCacheDictionary.GetOrAdd( type, t => MessagePackSerializer.Create( type ) ) != null;
        }

        public override Task<object> ReadFromStreamAsync( Type type, Stream readStream, HttpContent content,
            IFormatterLogger formatterLogger ) {

            if ( type == null ) {
                throw new ArgumentNullException( "type" );
            }
            if ( readStream == null ) {
                throw new ArgumentNullException( "readStream" );
            }

            return Task.Run( () => {

                try {

                    var unpacker = Unpacker.Create( readStream );
                    var unpackingObject = unpacker.Unpack<object>();
                    readStream.Close();
                    return unpackingObject;

                } catch ( Exception exception ) {
                    if ( formatterLogger == null ) {
                        throw;
                    } else {
                        formatterLogger.LogError( "MsgPack Read Async", exception.Message );
                    }
                }

                return GetDefaultValueForType( type );
            }, new CancellationToken() );
        }

        public override Task WriteToStreamAsync( Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext ) {

            if ( value == null ) {
                throw new ArgumentNullException( "value" );
            }
            if ( writeStream == null ) {
                throw new ArgumentNullException( "writeStream" );
            }

            return Task.Run( () => {

                try {

                    var packer = Packer.Create( writeStream );
                    packer.Pack( value );
                    writeStream.Flush();

                } catch ( Exception exception ) {

                    throw new NotSupportedException( "type", exception );
                }
            } );
        }
    }
}